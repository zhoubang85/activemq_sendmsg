package com.zb.mapper.sys;


import com.zb.bean.sys.SysMenu;
import tk.mybatis.mapper.common.Mapper;

public interface MenuMapper extends Mapper<SysMenu> {

}
