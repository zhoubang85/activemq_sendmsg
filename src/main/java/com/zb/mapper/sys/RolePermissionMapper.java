package com.zb.mapper.sys;


import com.zb.bean.sys.SysRolePermission;

import tk.mybatis.mapper.common.Mapper;

public interface RolePermissionMapper extends Mapper<SysRolePermission> {

}
