package com.zb.common.enums.entity;

public interface BaseEntityEnum<E extends Enum<E>> {

    int getIntValue();
}
